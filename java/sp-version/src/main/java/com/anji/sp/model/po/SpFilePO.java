package com.anji.sp.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author anji haitong teams
 * @since 2020-11-23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sp_file")
@ApiModel("文件管理")
public class SpFilePO extends BasePO {

    /** 文件性一id */
    @ApiModelProperty("文件性一id")
    private String fileId;

    /** 文件在linux中的完整目录，比如/app/file-sp/file/${fileid}.xlsx */
    @ApiModelProperty("文件在linux中的完整目录")
    private String filePath;

    /** 通过接口的下载完整http路径 */
    @ApiModelProperty("通过接口的下载完整http路径")
    private String urlPath;
}
