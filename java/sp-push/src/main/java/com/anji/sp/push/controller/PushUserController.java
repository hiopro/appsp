package com.anji.sp.push.controller;

import com.anji.sp.aspect.PreSpAuthorize;
import com.anji.sp.model.RequestModel;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.vo.PushUserVO;
import com.anji.sp.push.service.PushUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Api(tags = "推送历史")
@RestController
@RequestMapping("/pushUser")
public class PushUserController {

    @Autowired
    private PushUserService pushUserService;

    @PostMapping("/create")
    public ResponseModel create(@RequestBody PushUserVO requestModel) {
        return pushUserService.create(requestModel);
    }

    @PostMapping("/initUserInfo")
    public ResponseModel initUserInfo(@RequestBody PushUserVO requestModel) {
        return pushUserService.initUserInfo(requestModel);
    }

    @PostMapping("/updateById")
    public ResponseModel updateByDeviceId(@RequestBody PushUserVO requestModel) {
        return pushUserService.updateByDeviceId(requestModel);
    }

    @PostMapping("/deleteById")
    public ResponseModel deleteByDeviceId(@RequestBody PushUserVO requestModel) {
        return pushUserService.deleteByDeviceId(requestModel);
    }

    @PostMapping("/queryById")
    public ResponseModel queryByDeviceId(@RequestBody PushUserVO requestModel) {
        return pushUserService.queryByDeviceId(requestModel);
    }

    @PostMapping("/queryByPage")
    public ResponseModel queryByPage(@RequestBody PushUserVO requestModel) {
        return pushUserService.queryByPage(requestModel);
    }
}

