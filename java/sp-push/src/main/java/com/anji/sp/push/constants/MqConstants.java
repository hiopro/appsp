package com.anji.sp.push.constants;

/**
 * @Author: Kean
 * @Date: 2021/1/14
 * @Description:
 */
public class MqConstants {
    //交换器
    public static final String directExchange = "open.app_sp.push.exchange";

    //路由
    public static final String routing = "open.app_sp.push";

    //队列通道
    public final static String directBatchQueue = "open.app_sp.push.batch";
    public final static String directAllQueue = "open.app_sp.push.all";

}
