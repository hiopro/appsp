# 常见问题

## 后端
- 1. 后端服务部署 `ip2region.db` 需要放到`Nginx`上
然后将 sp-auth 模块下IPUntils.java 中 getCityInfo的dbPath替换成Nginx中ip2region.db的目录
- 2. 目录文件 `.properties`中`upload.filename`配置`/app/file-sp` <br/>
   服务器将创建文件目录`/app/file-sp/apk` 用来放apk使用 <br/>
   `file.apk.url`为下载地址Nginx配置虚拟下载地址<br/>
- 3. 行为验证码右下角logo乱码<br/>
    请参考：[AJ-Captcha Wiki](https://gitee.com/anji-plus/captcha/wikis/1.java后端接入-1.2.1版本后?sort_id=2308156) Wiki


## 前端
#### - 1. 浏览器兼容性问题
本项目暂时没有兼容性需求，如有兼容性需求可自行使用 babel-polyfill。
```javascript
// 下载依赖
npm install --save babel-polyfill
```
在入口文件中引入
```javascript
import 'babel-polyfill'
// 或者
require('babel-polyfill') //es6
```
在 webpack.config.js 中加入 babel-polyfill 到你的入口数组：
```javascript
module.exports = {
  entry: ['babel-polyfill', './app/js']
}
```
#### - 2.跨域问题 如：为什么发了一个 options 请求? Access-Control-Allow-Origin 报错等?
请参考文档 [跨域问题](https://panjiachen.github.io/vue-element-admin-site/zh/guide/advanced/cors.html)

#### - 3.业务模块访问出现404
确认菜单配置是否正确，菜单配置文件：@/src/router/index.js  
确认此角色是否已经配置菜单权限  