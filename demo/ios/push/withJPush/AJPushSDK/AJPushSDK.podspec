Pod::Spec.new do |s|
    s.name         = 'AJPushSDK'
    s.version      = '1.0.0'
    s.summary      = 'ios AJPush'
    s.homepage     = 'https://github.com/anji-plus/aj_ios_ajpush'
    s.license      = 'MIT'
    s.authors      = {'blackxu' => '747373635@qq.com'}
    s.platform     = :ios, '9.0'
    s.source       = {:git => 'https://github.com/anji-plus/aj_ios_ajpush.git', :tag => s.version}
    s.requires_arc = true
    s.source_files = 'AJPushSDK/*.{h,m}'
    s.dependency 'JCore', '2.1.4-noidfa'
    s.dependency 'JPush', '3.2.4-noidfa'
end
