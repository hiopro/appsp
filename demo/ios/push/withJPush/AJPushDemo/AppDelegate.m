//
//  AppDelegate.m
//  AJPushDemo
//
//  Created by Black on 2021/2/3.
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "SettingManager.h"
#import <AJPushSDK/AJPushSDK.h>

//#define APPSP_APPKEY @"fae48dd5c3834ff4aac9a12e79b0b481" //appsp推送 开发服务key
//#define APPSP_APPKEY @"368696029b144826b1db71dd08da790e" //appsp推送 测试服务key
#define APPSP_APPKEY @"3fffc7ab4e704965b3d7ebc034addef7" //appsp推送 uat服务key
#define APPSP_SECRETKEY @"12d606b4ab5b496992c48048e2a035f1" //appsp推送 uat服务Secret Key
#define JG_APPKEY @"66c9266fc53b8025cb0dd919" //极光推送服务key
#define JG_CHANNEL @"App Store" //极光推送通道
#define JG_IS_PRODUCTION 0 //0（默认值）表示采用的是开发证书，1 表示采用生产证书发布应用。
//#define AppSpTestURL @"http://appsp.dev.anji-plus.com" //dev
//#define AppSpTestURL @"http://appsp.test.anji-plus.com" //test
#define AppSpTestURL @"https://openappsp.anji-plus.com" //uat

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame: UIScreen.mainScreen.bounds];
    ViewController *vc = [ViewController new];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    
    //初始化配置信息
    [self initSettingData];
    //初始化推送服务
    [self initAPNSWithOptions:launchOptions];
    
    return YES;
}

//注册 APNs 成功并上报 DeviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"++application didRegisterForRemoteNotificationsWithDeviceToken");
    [[AJPushService shareService] registerDeviceToken:deviceToken withUrl:[SettingManager getAppSpUrl]];
}

//实现注册 APNs 失败接口
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"++did Fail To Register For Remote Notifications With Error: %@", error);
}

//初始化配置信息
- (void)initSettingData {
    if([SettingManager getJPushKey] == nil) {
        [SettingManager setJPushKey:JG_APPKEY];
    }
    if([SettingManager getAppSpKey] == nil) {
        [SettingManager setAppSpKey:APPSP_APPKEY];
    }
    if ([SettingManager getAppSpUrl] == nil) {
        [SettingManager setAppSpUrl:AppSpTestURL];
    }
    if ([SettingManager getAppSpSecretKey] == nil) {
        [SettingManager setAppSpSecretKey:APPSP_SECRETKEY];
    }
}

//初始化推送服务
- (void)initAPNSWithOptions:(NSDictionary *)launchOptions {
    [[AJPushService shareService] registerJPushOption:launchOptions appKey:[SettingManager getJPushKey] ajAppKey:[SettingManager getAppSpKey] channel:JG_CHANNEL apsForProduction:JG_IS_PRODUCTION];
}

@end
