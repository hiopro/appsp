package com.anji.plus.pushdemo.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.model.NotificationMessageModel;
import com.anji.plus.ajpushlibrary.util.CommonUtil;
import com.anji.plus.pushdemo.R;
import com.anji.plus.pushdemo.SoundUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 推送入口页面
 * </p>
 */
public class MainActivity extends AppCompatActivity {
    private MessageReceiver mMessageReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //注册接收透传消息的广播
        register();
    }

    private void register() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(AppSpPushConstant.MESSAGE_RECEIVED_ACTION);
        registerReceiver(mMessageReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
    }

    //接收透传消息的广播
    private class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, Intent intent) {
            try {
                if (AppSpPushConstant.MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                    Bundle bundle = intent.getExtras();
                    NotificationMessageModel notificationMessageModel = (NotificationMessageModel) bundle.getSerializable(AppSpPushConstant.HNOTIFICATIONMESSAGE);
                    final String title = notificationMessageModel.getTitle();
                    String messge = notificationMessageModel.getContent();
                    final String extras = notificationMessageModel.getNotificationExtras();
                    final StringBuilder showMsg = new StringBuilder();
                    showMsg.append(messge + "\n");
                    if (!CommonUtil.isEmpty(extras)) {
                        showMsg.append("extras: " + extras + "\n");
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buildDialog(title, showMsg.toString());
                            soundPlay(extras);
                        }
                    });
                }
            } catch (Exception e) {
            }
        }

        private void soundPlay(String extras) {
            //获取推送消息中的sound的值，注意sound时与后端协商好的传送声音类型的key，如果后端改变key，此处也随之改变
            JSONObject json = null;
            String soundType = "";//从推送消息中获取的播放的声音类型
            try {
                json = new JSONObject(extras);
                Iterator it = json.keys();
                while (it.hasNext()) {
                    String key = (String) it.next();
                    if (key.equals("sound")) {
                        soundType = json.get(key).toString();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //根据后端传来的数据值决定播放哪个声音
            switch (soundType) {
                case "hua_notice":
                    SoundUtils.play(1);
                    break;
                default:
                    SoundUtils.play(2);
            }
        }
    }

    private void buildDialog(String title, String msg) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this).setIcon(R.mipmap.ic_launcher).setTitle(title)
                .setMessage(msg).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "确定按钮", Toast.LENGTH_LONG).show();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "关闭按钮", Toast.LENGTH_LONG).show();
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }


}