
# **在线体验暂时下线！**


### 介绍
<div class="custom-block tip"> <p>AJ-Appsp由 <a href="http://www.anji-plus.com">安吉加加</a> 开源的App服务平台，如常用的 <span align="center" style="color:green">版本管理</span>、<span align="center" style="color:green">推送管理</span>，界面简洁优雅易用，我们提供Android、IOS、Flutter的SDK集成示例，提供管理后台的前后端源码，帮您快速构建企业内App常用功能。
我们专注做App公共服务，在移动领域为开发者赋能，减少重复造轮子的成本。AppSp完全开源，有详尽的开发和操作文档，我们将不遗余力，丰富AppSp功能，期待大家的使用并提出宝贵意见。
</p></div>


### 在线体验
**在线体验暂时下线**

### 功能介绍
* 应用管理：可以新增、删除、修改应用信息
    * 版本管理：核心功能, 对版本信息进行CRUD,版本更新主要分为历史版本更新、系统版本更新及灰度发布更新。
	* 推送管理：核心功能，以极光推送兜底，整合华为、小米、oppo、vivo厂商通道，即使应用进程杀死也能收到消息，提高消息抵达率， 
			    可进行推送测试、透传测试，可查看推送历史，。
    * 公告管理：公告管理是通过设置公告的持续时间及对应的公共模板，为APP分发不同的公共。
    * 成员管理：针对应用单独添加用户进入应用群组，还可单独配置每个用户管理该应用的版本、公告等权限信息。
* 账号管理：添加用户，为整个服务平台创建用户
* 文档集成：查看服务平台所有使用方法
* 基础设置：包含系统（iOS、Android）版本基础配置、公告模型基础配置、角色权限分配

### 核心功能
#### 版本更新 
* Android应用内apk更新
* Android应用外链接更新
* Android应用外H5页面更新
* IOS应用外App Store更新
* IOS应用外H5页面更新

#### 推送管理
* Android&iOS运行时消息推送
* Android&iOS运行时消息透传
* Android&iOS进程结束后消息推送
* Android&iOS推送历史

### 系统交互
appSp，app，app-api 三者之间的系统交互 

![组件](https://images.gitee.com/uploads/images/2021/1020/151911_249b528c_1600789.png)

### 技术栈

#### 前端

* `npm`: node.js的包管理工具，用于统一管理我们前端项目中需要用到的包、插件、工具、命令等，便于开发和维护。
* `webpack`: 用于现代 JavaScript 应用程序的_静态模块打包工具
* `vue-router`:  Vue提供的前端路由工具，利用其我们实现页面的路由控制，局部刷新及按需加载，构建单页应用，实现前后端分离。
* `element-ui`: 基于MVVM框架Vue开源出来的一套前端ui组件。
* `html2canvas`: 通过纯JS对浏览器端经行截屏
* `qrcodejs2`: 二维码生成插件


#### 后端

* `Spring Boot2.2.5.RELEASE`: Spring Boot是一款开箱即用框架，让我们的Spring应用变的更轻量化、更快的入门。 在主程序执行main函数就可以运行。你也可以打包你的应用为jar并通过使用java -jar来运行你的Web应用；
* `Spring Security`: Spring高度可定制的身份验证和访问控制框架。
* `Mybatis-plus3.3.2`: MyBatis-plus（简称 MP）是一个 MyBatis (opens new window) 的增强工具。
* `cn.jpush.api:jpush-client:3.4.7`: 极光推送服务端依赖。

#### Android（原生&Flutter）

* `cn.jiguang.sdk:jpush:3.9.0`: 极光推送客户端SDK。
* `cn.jiguang.sdk:jcore:2.6.0`: 极光推送客户端SDK。
* `com.huawei.hms:push:4.0.3.301`: 华为HMS推送套件。
* `MiPush_SDK_Client_3_8_5.jar`: 小米推送客户端SDK。
* `vivo_pushsdk-v2.9.0.0.aar`: vivo推送客户端SDK。
* `com.heytap.msp.aar`: oppo推送客户端SDK

#### iOS（原生&Flutter）

* JPush:3.2.4-noidfa: 极光推送客户端SDK。
* JCore:2.1.4-noidfa: 极光推送客户端SDK。

### 技术支持微信群
![09](https://images.gitee.com/uploads/images/2021/0810/103007_60cb17c4_1600789.jpeg)   


### 开源不易，劳烦各位star ☺



